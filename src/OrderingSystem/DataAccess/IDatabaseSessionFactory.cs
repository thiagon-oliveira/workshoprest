﻿using NHibernate;

namespace WorkshopRest.OrderingSystem.Api.DataAccess
{
    public interface IDatabaseSessionFactory
    {
        ISession Retrieve();
        void Close();
    }
}