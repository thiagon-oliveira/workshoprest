﻿using System.Collections.Generic;

namespace WorkshopRest.OrderingSystem.Api.Domain.Shared
{
    public interface IRepository
    {
        IList<dynamic> All<T>(int offset = 0, int limit = 20, Dictionary<string, SortOrder> sort = null,
            Dictionary<string, string> filters = null, string[] fields = null);

        T Get<T>(int id);
        void Add(IAggregateRoot<int> root);
        void Update(IAggregateRoot<int> root);
        void Remove(IAggregateRoot<int> root);
    }
}