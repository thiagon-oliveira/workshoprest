﻿using System.Collections.Generic;
using NHibernate;

namespace WorkshopRest.DeliverySystem.Api.DataAccess.Extensions
{
    public static class NHibernateExtensions
    {
        public static IList<dynamic> DynamicList(this ICriteria criteria)
        {
            return criteria.SetResultTransformer(NhTransformers.ExpandoObject)
                .List<dynamic>();
        }
    }
}