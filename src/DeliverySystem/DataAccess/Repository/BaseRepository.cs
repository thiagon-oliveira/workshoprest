﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Criterion;
using WorkshopRest.DeliverySystem.Api.DataAccess.Extensions;
using WorkshopRest.DeliverySystem.Api.Domain.Application;
using WorkshopRest.DeliverySystem.Api.Domain.Shared;
using ISession = NHibernate.ISession;

namespace WorkshopRest.DeliverySystem.Api.DataAccess.Repository
{
    public abstract class BaseRepository : IRepository
    {
        #region Properties

        protected ISession Session { get; set; }

        #endregion

        #region Constructors

        protected BaseRepository()
        {
            var session = (ISession) HttpContext.Current.Items["NHibernateSession"];

            if (session == null)
            {
                var sessionFactory = new DatabaseSessionFactory();
                session = sessionFactory.Retrieve();
            }

            Session = session;
        }

        protected BaseRepository(ISession session)
        {
            Session = session;
        }

        protected BaseRepository(IDatabaseSessionFactory dbSessionFatory)
            : this(dbSessionFatory.Retrieve())
        {
        }

        #endregion

        #region Public Methods

        public virtual T Get<T>(int id)
        {
            return Session.Get<T>(id);
        }

        public IList<dynamic> All<T>(int offset = 0, int limit = 20, Dictionary<string, SortOrder> sort = null,
            Dictionary<string, string> filters = null, string[] fields = null)
        {
            var criteria = Session.CreateCriteria(typeof (T));

            //criteria.SetMaxResults(limit);
            //criteria.SetFirstResult(offset);

            if (filters != null)
                SetFilters(filters, ref criteria);

            if (sort != null)
                SetSorting(sort, ref criteria);

            if (fields == null)
                return criteria.List<T>().Select(user => user.ToDynamic()).ToList();

            SetProjections(fields, ref criteria);

            return criteria.DynamicList();
        }

        public virtual void Add(IAggregateRoot<int> root)
        {
            Session.SaveOrUpdate(root);
        }

        public virtual void Update(IAggregateRoot<int> root)
        {
            Session.SaveOrUpdate(root);
        }

        public virtual void Remove(IAggregateRoot<int> root)
        {
            Session.Delete(root);
        }

        #endregion

        #region Private Methods

        private static void SetFilters(Dictionary<string, string> filters, ref ICriteria criteria)
        {
            foreach (var filter in filters)
            {
                var propertyType = criteria.GetRootEntityTypeIfAvailable().GetProperty(filter.Key).PropertyType;
                var typeConverter = TypeDescriptor.GetConverter(propertyType);

                criteria.Add(Restrictions.Eq(filter.Key, typeConverter.ConvertFromString(filter.Value)));
            }
        }

        private static void SetSorting(Dictionary<string, SortOrder> sort, ref ICriteria criteria)
        {
            foreach (var s in sort)
            {
                if (s.Value == SortOrder.Ascending)
                {
                    criteria.AddOrder(new Order(s.Key, true));
                    continue;
                }
                criteria.AddOrder(new Order(s.Key, false));
            }
        }

        private static void SetProjections(string[] fields, ref ICriteria criteria)
        {
            var projections = Projections.ProjectionList();

            foreach (var field in fields)
                projections.Add(Projections.Property(field), field);

            criteria.SetProjection(projections);
        }

        #endregion
    }
}