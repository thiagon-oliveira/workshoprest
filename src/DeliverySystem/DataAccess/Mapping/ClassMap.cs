﻿namespace WorkshopRest.DeliverySystem.Api.DataAccess.Mapping
{
    public class ClassMap<T> : FluentNHibernate.Mapping.ClassMap<T>
    {
        protected ClassMap()
        {
            CacheSchema();
        }

        protected ClassMap(string schema, string tableName) : this()
        {
            Schema(schema);
            Table(tableName);
        }

        protected void CacheSchema()
        {
            Cache.ReadWrite();
        }
    }
}