﻿using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;
using NHibernate.Tool.hbm2ddl;
using WorkshopRest.DeliverySystem.Api.DataAccess.Mapping;

namespace WorkshopRest.DeliverySystem.Api.DataAccess
{
    public class DatabaseSessionFactory : IDatabaseSessionFactory
    {
        #region Fields

        private readonly object _threadSafe = new object();

        #endregion

        #region Constructors

        public DatabaseSessionFactory()
        {
            var exposeConfiguration = Fluently.Configure()
                .Database(MsSqlCeConfiguration.MsSqlCe40.ConnectionString(EnvironmentSettings.ConnectionString))
                .Mappings(m =>
                {
                    m.FluentMappings.AddFromAssemblyOf<ClassMap<>>();
                    m.FluentMappings.Conventions.Add<EnumConvention>();
                }
                )
                .ExposeConfiguration(cfg =>
                {
                    cfg.SetProperty("current_session_context_class", "web");
                    cfg.SetProperty("command_timeout", "360");
                    new SchemaExport(cfg).Create(true, false);
                });

            SessionFactory = exposeConfiguration.BuildSessionFactory();
        }

        #endregion

        #region Properties

        public ISessionFactory SessionFactory { get; }
        public ISession Session { get; private set; }

        #endregion

        #region Public Methods

        public ISession Retrieve()
        {
            if (CurrentSessionContext.HasBind(SessionFactory))
            {
                Session = SessionFactory.GetCurrentSession();
            }
            else
            {
                Session = SessionFactory.OpenSession();
                CurrentSessionContext.Bind(Session);
            }

            return Session;
        }

        public void Close()
        {
            lock (_threadSafe)
            {
                Session.Dispose();

                var session = CurrentSessionContext.Unbind(SessionFactory);

                if (session == null || !session.IsOpen) return;

                try
                {
                    if (session.Transaction != null && session.Transaction.IsActive)
                    {
                        session.Transaction.Rollback();
                        throw new Exception("Rolling back uncommited NHibernate transaction.");
                    }

                    session.Flush();
                }
                finally
                {
                    session.Close();
                    session.Dispose();
                }
            }
        }

        #endregion
    }
}