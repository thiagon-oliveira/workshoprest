﻿using NHibernate;

namespace WorkshopRest.DeliverySystem.Api.DataAccess
{
    public interface IDatabaseSessionFactory
    {
        ISession Retrieve();
        void Close();
    }
}