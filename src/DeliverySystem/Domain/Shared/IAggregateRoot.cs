﻿namespace WorkshopRest.DeliverySystem.Api.Domain.Shared
{
    public interface IAggregateRoot<T>
    {
        T Id { get; set; }
    }
}