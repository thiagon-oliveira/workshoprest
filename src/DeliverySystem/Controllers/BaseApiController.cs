﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WorkshopRest.DeliverySystem.Api.Model;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WorkshopRest.DeliverySystem.Api.Controllers
{

    public abstract class BaseApiController : Controller
    {
        public abstract IEnumerable<dynamic> Get(GetModel model);
    }
}
