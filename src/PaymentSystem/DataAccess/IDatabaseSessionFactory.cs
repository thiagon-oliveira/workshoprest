﻿using NHibernate;

namespace WorkshopRest.PaymentSystem.Api.DataAccess
{
    public interface IDatabaseSessionFactory
    {
        ISession Retrieve();
        void Close();
    }
}