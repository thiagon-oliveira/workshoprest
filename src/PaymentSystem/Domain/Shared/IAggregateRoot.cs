﻿namespace WorkshopRest.PaymentSystem.Api.Domain.Shared
{
    public interface IAggregateRoot<T>
    {
        T Id { get; set; }
    }
}