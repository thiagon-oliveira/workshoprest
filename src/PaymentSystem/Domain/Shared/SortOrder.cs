﻿namespace WorkshopRest.PaymentSystem.Api.Domain.Shared
{
    public enum SortOrder
    {
        Unspecified = -1,
        Ascending = 0,
        Descending = 1
    }
}